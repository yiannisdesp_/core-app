    (function() {
    var canvas = document.querySelector('#paint');
    var canvas_img_data = "";

    var ctx = canvas.getContext('2d');

    var sketch = document.querySelector('#sketch');
    var reset = document.querySelector('#reset');

    var sketch_style = getComputedStyle(sketch);
    canvas.width = parseInt(sketch_style.getPropertyValue('width'));
    canvas.height = parseInt(sketch_style.getPropertyValue('height'));

    var imageObj = new Image();
    imageObj.src = '../images/car_sides.png';
    imageObj.onload = function() {
        ctx.drawImage(imageObj,25,25,650,350);
    };
  

    var mouse = {x: 0, y: 0};
    var last_mouse = {x: 0, y: 0};

    /* Mouse Capturing Work */
    canvas.addEventListener('mousemove', function(e) {
        last_mouse.x = mouse.x;
        last_mouse.y = mouse.y;

        mouse.x = e.pageX - this.offsetLeft;
        mouse.y = e.pageY - this.offsetTop;
    }, false);

    init_drawing();
    /* Drawing on Paint App */
    function init_drawing(){
        ctx.lineWidth = 5;
        ctx.lineJoin = 'round';
        ctx.lineCap = 'round';
        ctx.strokeStyle = 'red';
        imageObj.src = '../images/car_sides.png';
        ctx.drawImage(imageObj,25,25,650,350);
    }
    
     
    canvas.addEventListener('mousedown', function(e) {
        canvas.addEventListener('mousemove', onPaint, false);
    }, false);

    canvas.addEventListener('mouseup', function() {
        canvas.removeEventListener('mousemove', onPaint, false);
        save_canvas();
    }, false);

    reset.addEventListener('mousedown', function(){
        canvas.width = canvas.width;
        $("#canvas_output").val("");
        canvas_img_data = "";
        init_drawing();
    }, false);

    var onPaint = function() {
        ctx.beginPath();
        ctx.moveTo(last_mouse.x, last_mouse.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.closePath();
        ctx.stroke();
    };

    function save_canvas() {
        canvas_img_data = canvas.toDataURL( "image/png");
        $("#canvas_output").val(canvas_img_data);
    }

    

}());