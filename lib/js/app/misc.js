/**
*
* Load tinyMCE function with params
*
**/
/* params => height, width, theme */
function init_tiny_mce(height_,width_,theme_,selector_){
	tinymce.init({
	    selector: selector_,
	    theme: theme_,
	    plugins: [
	        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	        "searchreplace wordcount visualblocks visualchars code fullscreen",
	        "insertdatetime media nonbreaking save table contextmenu directionality",
	        "template paste textcolor colorpicker textpattern"
	    ],
	    width: width_,
	    height: height_,
	    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    toolbar2: "print preview media | forecolor backcolor",
	    image_advtab: true,
	    entity_encoding : "raw"
	});
}
