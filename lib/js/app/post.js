/**
*
* Change language request
*
**/
$(document).ready(function(){

	$('.lang-change').click(function(){
		var lang = ($(this).attr('href').toLowerCase()).replace('#','');
		$.post('ajax-request/', { 'chng_lang':lang }, function(){
		}).done(function(){
			location.reload();
		});
	});

});