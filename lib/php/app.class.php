<?php

class app{

	/* Public db instance */
	public $db;
	/* Public content instance */
	public $content;
	/* Public pagination instance */
	private $pagination;

	/*==========  App Constructor  ==========*/
	public function __construct(){
		// load database framework: medoo
		$this->load_class('database/database.class.php');
		// create database object
		$this->db = new database();
	}

	## Set content
	public function set_content(){
		//load content class
		$this->load_class('content/content.class.php');
		$this->content = new content();
	}

	## Load class
	public function load_class($path){
		require_once($path);
	}

	## Stop application and print a message as error
	public function fatal_error($str){
		$html = "<div style='padding:10px;border:3px solid red;color:red'>App Error<br/><br/>".$str."</div>";
		die($html);
	}

	## Define settings as constants to be used by all php files
	public function load_site_config(){
		// Define website setttings
		$settings = $this->db->select('config','*');
		foreach($settings as $setting){
			define(strtoupper($setting['setting']), $setting['value']);
		}
	}

	## Debug
	public function debug_me($var, $dump = 0){
		echo "<pre>";
		if($dump) var_dump($var); else print_r($var);
		echo "</pre>";
	}

	## Redirect to:
	public function redirect_to($link){
		header('Location: ' . $link);
		return false;
	}

	## Show/Hide php errors/warnings
	public function display_errors($flag = true){
		if($flag){
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}else{
			error_reporting(E_ERROR);
		}
	}

	## Set timezone
	public function set_time_zone($timezone){
		date_default_timezone_set($timezone);
	}

	## Database backup - export sql file - Methods: 1. 'download' (default) 2. 'store'
	public function backup_db($method = 'download'){
		$this->load_class('db_backup/db_backup.class.php');
		$filename = "db_backup_".date("d-m-Y_H_i_s").".sql";
		if($method == 'store'){
			$backup = new db_backup(DB_BACKUP_DIR,$filename);
			$backup->store_backup();
		}else{
			$backup = new db_backup("",$filename);
			$backup->download_backup();
		}
	}

	## Generate breadcrumbs
	function breadcrumbs($params = array()) {

		if(isset($params['separator'])) $separator = $params['separator']; else $separator = ' ' . BREADCRUMB_SEPARATOR . ' ';
		if($this->url_contains('admin')){
			if(isset($params['home'])) $home = $params['home']; else $home = ADMIN_BREADCRUMB_HOME;
			if(isset($params['anchor_class'])) $anchor_class = $params['anchor_class']; else $anchor_class = ADMIN_BREADCRUMB_ANCHOR_CLASS;
			if(isset($params['div_class'])) $div_class = $params['div_class']; else $div_class = ADMIN_BREADCRUMB_DIV_CLASS;
		}else{
			if(isset($params['home'])) $home = $params['home']; else $home = BREADCRUMB_HOME;
			if(isset($params['anchor_class'])) $anchor_class = $params['anchor_class']; else $anchor_class = BREADCRUMB_ANCHOR_CLASS;
			if(isset($params['div_class'])) $div_class = $params['div_class']; else $div_class = BREADCRUMB_DIV_CLASS;
		}
		
		$path = $_SERVER["PHP_SELF"];
		$parts = explode('/',$path);
		echo "<div class='".$div_class."' style='padding:13px'>";
		if (count($parts) < 2){
			echo($home);
		}else{
			echo ("<a class='".$anchor_class."' href='".ADMIN_ABS_URL."'>".$home."</a> &raquo; ");
			for ($i = 1; $i < count($parts); $i++){
			    if (!strstr($parts[$i],".")){
			        echo("<a class='".$anchor_class."' href='");
			        for ($j = 0; $j <= $i; $j++) {echo $parts[$j]."/";};
			        echo("'>". str_replace('-', ' ', $parts[$i])."</a> &raquo; ");
			    }else{
			        $str = $parts[$i];
			        $pos = strrpos($str,".");
			        $parts[$i] = substr($str, 0, $pos);
			        echo str_replace('-', ' ', $parts[$i]);
			    }
			}
		}
		echo "</div>";
	}

	## Read directory
	public function get_dir_contents($dir){
		$dir = $dir;
		$files = scandir($dir);
		// unset the . (current) and .. (back) dir
		unset($files[0]);
		unset($files[1]);
		// reorder array
		$files_ = array();
		foreach ($files as $file) {
			$files_[] = $file;
		}
		return $files_;
	}

	## Delete file
	public function remove_file($dir){
		if(is_readable($dir)){
			unlink($dir);
			return true;
		}else{
			return false;
		}
	}

	## Search db  - query string
	public function search_db($query_string, $date_from = 0, $date_to = 0){
		header('Content-Type:text/html; charset=UTF-8');
		$keywords = mysqli_real_escape_string($this->db_connection,$query_string);
		$keywords = str_replace("'","",$keywords);
		$keywords = str_replace('"',"",$keywords);
		$keywords_array = explode(" ",$keywords);
		$keywords_array_size = count($keywords_array);
		$array_of_fields = array('invoice_title','customer_id','car_model','car_brand','car_year','license_plate','customers.comments','services.comments','extra_cost','name','surname','address','email','phone_no','invoice_html_en','invoice_html_el');
		$array_of_fields_size = count($array_of_fields);
		$sql = "SELECT *,customers.active AS customer_active, services.active AS service_active, invoices.active AS invoice_active, customers.comments AS customer_comment, services.comments AS service_comments, services.created AS service_created, invoices.id AS invoice_id
				FROM `services`
				INNER JOIN customers ON customers.id = services.customer_id
				INNER JOIN invoices ON services.id = invoices.service_id WHERE (";
		//SET COUNTERS + THE LENGTHS OF $array_of_fields AND THE $keywords_array
		$a = 0;
		$b = 0;
		//START LOOP
		while ($a<$keywords_array_size) {//IF COUNTER $a < KEYWORDS DO
		  while ($b<$array_of_fields_size) { //IF COUNTER $b < NUMBER OF FIELDS DO
			  $sql = $sql."$array_of_fields[$b] LIKE '%$keywords_array[$a]%'";//ALL FIELDS LIKE KEYWORD IN ARRAY[$a]
			  $b++;//INCREASE COUNTER BY 1
			  if ($b<$array_of_fields_size) {//CHECK IF COUNTER $b < NUMBER OF FIELDS
				  $sql = $sql." OR ";//IF TRUE, PRINT " OR "
			  }
		  }//END INNER WHILE LOOP
		 $a++;//INCREASE COUNTER $a BY 1
		 if ($a<$keywords_array_size) {//CHECK IF COUNTER $a < NUMBER OF KEYWORDS
			 $sql = $sql.") AND ("; //IF TRUE, PRINT ") AND ( "
		 }
		 $b=0;// RESET THE SECOND COUNTER
		}//END OUTER WHILE LOOP
		$sql .= ") AND services.sys_user_id = ".$_COOKIE['uid']." AND customers.active=1 AND services.active=1 AND invoices.active=1";
		if(!($date_from == 0 && $date_to == 0)){
			$sql .= " AND (services.created BETWEEN '".$date_from."' AND '".$date_to."')";
		}
		$result = $this->query_db($sql);
		return $result;
	}

	## Check if string contains substring
	public function string_contains($string,$substring){
		if (false !== strpos($string, $substring)) {
		    return true;
		} else {
		    return false;
		}
	}

	## Make a database field readable
	public function readable_field($str){
		return ucfirst(str_replace('_',' ', $str));
	}

	## Check string length
	public function if_string_empty($string){
		if(strlen($string) == 0) return true; else return false;
	}

	## Check if url contains certain string
	public function url_contains($string){
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		if($this->string_contains($url,$string)){
			return true;
		}else{
			return false;
		}
	}

	## Paginate array
	public function paginate_array($array,$records_per_page){
		$this->load_class('zebra_pagination');
		// instantiate the pagination object
		$this->pagination = new Zebra_Pagination();
		// the number of total records is the number of records in the array
		$this->pagination->records(count($array));
		// records per page
		$this->pagination->records_per_page($records_per_page);
		// here's the magick: we need to display *only* the records for the current page
		$array = array_slice(
		    $array,
		    (($this->pagination->get_page() - 1) * $records_per_page),
		    $records_per_page
		);
		return $array;
	}

	## Render pagination numbers
	public function render_pagination_numbers($return_output = false){
		$this->pagination->render($return_output);
	}

	## ================================================= Notifications
	## Notification messages
	public function alert_box($msg, $notification_type = 'error', $behave = 'print'){
		switch($notification_type){
			case 'error': {
				$html = "<div class='alert-box error'>";
				$html .= "<span>error: </span>".$msg."</div>";
				break;
			}
			case 'warning': {
				$html = "<div class='alert-box warning'>";
				$html .= "<span>warning: </span>".$msg."</div>";
				break;
			}
			case 'success': {
				$html = "<div class='alert-box success'>";
				$html .= "<span>success: </span>".$msg."</div>";
				break;
			}
			case 'notice': {
				$html = "<div class='alert-box notice'>";
				$html .= "<span>notice: </span>".$msg."</div>";
				break;
			}
			default: {
				$html = "<div class='alert-box error'>";
				$html .= "<span>error: </span>".$msg."</div>";
				break;
			}
		} ## Switch end
		if($behave == 'print'){
			echo $html;
		}elseif ($behave == 'get'){
			return $html;
		}else{
			echo "notify_msg( $msg, $behave = 'print', ['print','get'], $notification_type = 'error', ['error','warning','success','notice'])";
		}
	}

	#=================================================== App's authentication ##
	## Authentication function, return true or false
	## Parameters: $auth method: 'cookie', 'session'
	public function authenticate_user($auth_type = 'cookie'){
		switch($auth_type){
			case 'cookie': {
				if(isset($_COOKIE["uid"]) && isset($_COOKIE["identifier"])){
					$uid = $_COOKIE["uid"];
					$identifier = $_COOKIE["identifier"];
				}
				break;
			}
			case 'session': {
				if(isset($_SESSION["uid"]) && isset($_SESSION["identifier"])){
					$uid = $_SESSION["uid"];
					$identifier = $_SESSION["identifier"];
				}
				break;
			}
			default:{
				if(isset($_COOKIE["uid"]) && isset($_COOKIE["identifier"])){
					$uid = $_COOKIE["uid"];
					$identifier = $_COOKIE["identifier"];
				}
				break;
			}
		}
		if(isset($uid) && isset($identifier) && $uid != NULL && $identifier != NULL){
			$user_data = $this->push_array_up($this->db->select('users',array('id','email','password'),array('id'=>$uid)));
			if(!empty($user_data)){
				$identifier_ = sha1($user_data['email'].$user_data['password']);
				if($user_data['id'] == $uid && $identifier_ == $identifier){
					$this->set_auth($user_data['id'],$user_data['email'],$user_data['password'],$auth_type);
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


	## Set user identification info
	public function set_auth($user_id,$email,$password,$auth_type){
		// set cookies last 24 hour
		// syntax: setcookie(name,value,expire,path,domain,secure)
		switch($auth_type){
			case 'cookie': {
				setcookie("uid",$user_id,time()+3600*24,COOKIES_PATH);
				setcookie("identifier",sha1($email.$password),time()+3600*24,COOKIES_PATH);
				break;
			}
			case 'session': {
				$_SESSION["uid"] = $user_id;
				$_SESSION["identifier"] = sha1($email.$password);
				break;
			}
			default:{
				setcookie("uid",$user_id,time()+3600*24,COOKIES_PATH);
				setcookie("identifier",sha1($email.$password),time()+3600*24,COOKIES_PATH);
				break;
			}
		}
		
	}

	## Logout app
	public function logout($auth_type = 'cookies'){
		// make a db backup upon every exit
		//$this->backup_db('store',ABS_PATH."backup/db_backups");
		switch($auth_type){
			case 'cookies': {
				if(isset($_COOKIE['uid'])){ $user_id = $_COOKIE['uid']; unset($_COOKIE['uid']);}
				if(isset($_COOKIE['identifier'])){ unset($_COOKIE['identifier']);}
				setcookie('user', null, -1, COOKIES_PATH);
		        setcookie('identifier', null, -1, COOKIES_PATH);
				break;
			}
			case 'session': {
				if(isset($_SESSION['uid'])){ $user_id = $_SESSION['uid']; unset($_SESSION['uid']);}
				if(isset($_SESSION['identifier'])){ unset($_SESSION['identifier']);}
				break;
			}
			default:{
				if(isset($_COOKIE['uid'])){ $user_id = $_COOKIE['uid']; unset($_COOKIE['uid']);}
				if(isset($_COOKIE['identifier'])){ unset($_COOKIE['identifier']);}
				setcookie('user', null, -1, COOKIES_PATH);
		        setcookie('identifier', null, -1, COOKIES_PATH);
				break;
			}
		}
		//update user status:
		$this->db->update('users',array('session_active'=>0,),array('id'=>$user_id));
        //close db connection
        $this->db->close();
        return true;
	}

	## Sign in user for web browsing, return true or false
	public function login($email, $password, $auth_type = 'cookies'){
		// Get user data if match found, then push up array elements one level up
		$user_data = $this->push_array_up($this->db->select('users','*',['AND'=>['email'=>$email,'password'=>sha1($password)]]));
		if(!empty($user_data)){
			$this->set_auth($user_data['id'],$email,sha1($password),$auth_type);
			//update user stats:
			$date = date('Y-m-d H:i:s');
			$this->db->update('users',
				array(
				'last_login'=>$date,
				'last_login_ip'=>$this->get_client_ip(),
				'session_active'=>1,
				'total_logins[+]'=>1
				),
				array(
					'id'=>$user_data['id']
				));
			return true;
		}else{
			return false;
		}
	}

	## Get logged in user id:
	public function get_logged_in_user_id(){
		if(isset($_COOKIE["uid"]) && isset($_COOKIE["identifier"]) 
		&& $_COOKIE["uid"] != NULL && $_COOKIE["identifier"] != NULL){
			$userID = $_COOKIE["uid"];
			return $userID;
		}elseif(isset($_SESSION["uid"]) && isset($_SESSION["identifier"]) 
		&& $_SESSION["uid"] != NULL && $_SESSION["identifier"] != NULL){
			$userID = $_SESSION["uid"];
			return $userID;
		}else{
			return false;
		} 
	}
	## Get logged in user detail
	public function logged_in_user($field = '*'){
		if($field == 'id'){
			return $this->get_logged_in_user_id();
		}elseif($field == '*'){
			return $this->push_array_up($this->db->select('users','*',array('id'=>$this->get_logged_in_user_id())));
		}else{
			return $this->push_array_up($this->db->select('users',$field,array('id'=>$this->get_logged_in_user_id())));
		}
	}

	## Push array one level up
	public function push_array_up($array){
		return $array[0];
	}


	## Email validation, returns true or false
	public function validate_email($email){
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}else{
			return false;
		}
	}

	## String functions
	function starts_with($haystack, $needle){
	    return $needle === "" || strpos($haystack, $needle) === 0;
	}
	function ends_with($haystack, $needle){
	    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}

	## Send post data
	public function post_data($url,$data){
		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data),
		    ),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		var_dump($result);
	}

	## Get client ip address
	function get_client_ip() {
	    $ipaddress = '';
	    $ipaddress = getenv('HTTP_CLIENT_IP')?:
		getenv('HTTP_X_FORWARDED_FOR')?:
		getenv('HTTP_X_FORWARDED')?:
		getenv('HTTP_FORWARDED_FOR')?:
		getenv('HTTP_FORWARDED')?:
		getenv('REMOTE_ADDR');
	    return $ipaddress;
	}

} // End of app class

?>