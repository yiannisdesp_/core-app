<?php
class ga extends app{

	private $client;

	public function __construct(){
		parent::__construct();
	}

	public function print_script(){
		$script = '<script>';
		$script .= '(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){';
		$script .= '(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),';
		$script .= 'm=s.getElementsByTagName(o)';
		$script .= '[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)';
		$script .= '})(window,document,"script","//www.google-analytics.com/analytics.js","ga");';
		$script .= 'ga("create", "'.constant('GA_TRACKING_CODE').'", "auto");';
		$script .= 'ga("send", "pageview");';
		$script .= '</script>';
		echo $script;
	}

	public function access_api(){
		$this->load_class('ga/Client.php');
		$this->load_class('ga/Service/Books.php');
		$this->client = new Google_Client();
		$this->client->setApplicationName("Client_Library_Examples");
  		$this->client->setDeveloperKey("YOUR_APP_KEY");
	}
}

?>