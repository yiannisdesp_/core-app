<?php

class content extends app{

	## language
	public $_lang;

	public function __construct($lang = ''){
		parent::__construct();
		## If default lang is not set from constructor, set the default
		if($lang == ''){
			if(isset($_COOKIE['lang'])){
				/* check if language exists in db */
				if(in_array($_COOKIE['lang'], $this->get_languages())){
					$this->_lang = $_COOKIE['lang'];
				}else{
					$this->_lang = DEFAULT_LANG;
				}
			}else{
				// if default language is not set, then set the first one met
				if(defined(DEFAULT_LANG)){
					if(DEFAULT_LANG !== ''){
						$this->_lang = DEFAULT_LANG;
					}else{
						$langs = $this->get_languages();
						$this->_lang = $langs[0];
					}
				}else{
					$langs = $this->get_languages();
					$this->_lang = $langs[0];
				}
			}
			// Load all content
			$this->load_all_content();
		}else{
			$this->_lang = $lang;
		}
		
	}

	## Get all available languages from database
	public function get_languages(){
		$not_langs = array('id','keyword','table_name','table_id','large_text');
		$lang_fields = array();
		$db_info = $this->db->get_db_credentials();
		$content_fields = $this->db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$db_info['db_name']."' AND TABLE_NAME='content'")->fetchAll();
		foreach ($content_fields as $content) {
			if(!in_array($content['COLUMN_NAME'], $not_langs)){
				$lang_fields[] = $content['COLUMN_NAME'];
			}
		}
		return $lang_fields;
	}

	## Get translation by language and keyword
	public function get_content_by_keyword($keyword, $lang = null){
		if($lang == null) { $lang = $this->_lang; }
		$translation = $this->db->select('content',$lang,array('keyword'=>$keyword));
		$translation = $this->push_array_up($translation);
		return $translation;
	}

	## Get translation by language, table name and table id
	public function get_content_by_table($table_name, $table_id, $lang = null){
		if($lang == null) { $lang = $this->_lang; }
		$translation = $this->db->select('content',$lang,array('AND'=>array('table_name'=>$table_name,'table_id'=>$table_id)));
		$translation = $this->push_array_up($translation);
		return $translation;
	}

	## Get all translations by keyword
	public function get_translations($keyword){
		$translations = $this->db->select('content',$this->get_languages(),array('keyword'=>$keyword));
		if(!empty($translations)){
			$translations = $this->push_array_up($translations);
			return $translations;
		}else{
			$this->alert_box('Keyword: ' . $keyword . ' not found.','error');
			return false;
		}
	}

	## Change language
	public function set_lang($lang){
		/* set language cookie */
		// syntax: setcookie(name,value,expire,path,domain,secure)
		if(in_array($lang, $this->get_languages()))
			setcookie("lang",$lang,time()+3600*24,COOKIES_PATH); // expires after 24 hours
	} 

	## Load all content from db
	public function load_all_content(){
		$content = $this->db->select('content',array('keyword',$this->_lang));
		$temp = array();
		foreach ($content as $value) {
			$temp[$value['keyword']] = $value[$this->_lang];
		}
		$content = $temp;
		unset($temp);
		/* set content as a constan array, use it later as: $content = unserialize(CONTENT) */
		define("CONTENT", serialize($content));
	}

}

?>