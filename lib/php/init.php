<?php

	/*===========================================
	=           App v0.0.1 - 1/10/2014 			=
	=			Yiannis Despotis				=
	==========================================*/

	/*===========================================
	=            App initialization             =
	===========================================*/
	
	/*==========  Create a new app instance stored in $app  ==========*/
	require_once('app.class.php');
	$app = new app();

	/*==========  Load all settings from config table as constants  ==========*/
	$app->load_site_config();

	/*==========  Start session in admin pages  ==========*/
	if($app->url_contains('admin')){
		if(!isset($_SESSION)) session_start();
	}

	/*==========  Set timezone  ==========*/
	$app->set_time_zone('Europe/Athens');

	/*==========  Enable errors/warnings  ==========*/
	$app->display_errors();

	/*==========  Create a new content instance inside app class  ==========*/
	/**
	*
	* Use of content: $app->content->function()
	*
	**/
	$app->set_content();
	/* Access content translations or simply the text from the following array variable as $_content['keyword'] */
	$site_txt = unserialize(CONTENT);
	
?>