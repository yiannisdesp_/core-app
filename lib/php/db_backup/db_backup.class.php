<?php
class db_backup extends app{

	#############################
	##							#
	##    Mysqli connection 	#
	##   Database full backup 	#
	##							#
	#############################

	private $filename;
	private $backup_folder;

	private $mysqli;
	private $db_host;
	private $db_name;
	private $db_password;
	private $db_username;

	public function __construct($backup_folder = DB_BACKUP_DIR, $filename = "backup_.sql"){
		parent::__construct();
		$this->backup_folder = $backup_folder;
		$this->filename = $filename;
		$this->load_db_credentials();
		$this->connect();
	}

	private function load_db_credentials(){
		$data = $this->db->get_db_credentials();
		$this->db_host = $data['db_host'];
		$this->db_name = $data['db_name'];
		$this->db_username = $data['db_user'];
		$this->db_password = $data['db_pass'];
	}

	private function connect(){
		$this->mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_name);
		if (mysqli_connect_error()) {
			$error = 'Connection Error (' . mysqli_connect_errno() . ') '.mysqli_connect_error();
		   	$this->fatal_error($error);
		}
	}

	public function create_backup(){
		try {
			$sql_dump =  "";
			// put a few comments into the SQL file
	        $sql_dump .=  ("-- pjl SQL Dump\n");
	        $sql_dump .=  ("-- Server version:".$this->mysqli->server_info."\n");
	        $sql_dump .=  ("-- Generated: ".date('Y-m-d h:i:s')."\n");
	        $sql_dump .=  ('-- Current PHP version: '.phpversion()."\n");
	        $sql_dump .=  ('-- Host: '.$this->db_host."\n");
	        $sql_dump .=  ('-- Database:'.$this->db_name."\n");
	        //get a list of all the tables
	        $aTables = array();
	        $strSQL = 'SHOW TABLES';// I put the SQL into a variable for debuggin purposes - better that "check syntax near '), "
	        if (!$res_tables = $this->mysqli->query($strSQL))
	            throw new Exception("MySQL Error: " . $this->mysqli->error . 'SQL: '.$strSQL);

	        while($row = $res_tables->fetch_array()) {
	            $aTables[] = $row[0];
	        }
	        // Don't really need to do this (unless there is loads of data) since PHP will tidy up for us but I think it is better not to be sloppy
	        // I don't do this at the end in case there is an Exception
	        $res_tables->free();
	        //now go through all the tables in the database
	        foreach($aTables as $table)
	        {
	            $sql_dump .=  ("-- --------------------------------------------------------\n");
	            $sql_dump .=  ("-- Structure for '". $table."'\n");
	            $sql_dump .=  ("--\n\n");

	            // remove the table if it exists
	            $sql_dump .=  ('DROP TABLE IF EXISTS '.$table.';');

	            // ask MySQL how to create the table
	            $strSQL = 'SHOW CREATE TABLE '.$table;
	            if (!$res_create = $this->mysqli->query($strSQL))
	                throw new Exception("MySQL Error: " . $mysqli->error . 'SQL: '.$strSQL);
	            $row_create = $res_create->fetch_assoc();

	            $sql_dump .=  ("\n".$row_create['Create Table'].";\n");
	            $sql_dump .=  ("-- --------------------------------------------------------\n");
	            $sql_dump .=  ('-- Dump Data for `'. $table."`\n");
	            $sql_dump .=  ("--\n\n");
	            $res_create->free();

	            // get the data from the table
	            $strSQL = 'SELECT * FROM '.$table;
	            if (!$res_select = $this->mysqli->query($strSQL))
	                throw new Exception("MySQL Error: " . $this->mysqli->error . 'SQL: '.$strSQL);

	            // get information about the fields
	            $fields_info = $res_select->fetch_fields();

	            // now we can go through every field/value pair.
	            // for each field/value we build a string strFields/strValues
	            while ($values = $res_select->fetch_assoc()) {

	                $strFields = '';
	                $strValues = '';
	                foreach ($fields_info as $field) {
	                    if ($strFields != '') $strFields .= ',';
	                    $strFields .= "`".$field->name."`";

	                    // put quotes round everything - MYSQL will do type convertion (I hope) - also strip out any nasty characters
	                    if ($strValues != '') $strValues .= ',';
	                    $strValues .= '"'.preg_replace('/[^(\x20-\x7F)\x0A]*/','',$values[$field->name].'"');
	                }

	                // now we can put the values/fields into the insert command.
	                $sql_dump .=  ("INSERT INTO ".$table." (".$strFields.") VALUES (".$strValues.");\n");
	            }
	            $sql_dump .=  ("\n\n\n");
	            $res_select->free();
	        }
	    } catch (Exception $e) {
	        $sql_dump .=  ($e->getMessage());
	    }
	    return $sql_dump;
	}

	## Direct download of sql file
	public function download_backup(){
		ob_start();
		header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Type: application/force-download');
        header('Content-Type: application/octet-stream');
        header('Content-Type: application/download');
        header('Content-Disposition: attachment;filename="'.$this->filename.'".gz');
        header('Content-Transfer-Encoding: binary');
        $f_output = fopen("php://output", 'w');
        $gzdata = gzencode($this->create_backup(), 9);
        fwrite($f_output,$gzdata);
	    fclose($f_output);
	    $this->mysqli->close();
	}

	## Store sql file
	public function store_backup(){
		ob_start();
		$f_output = fopen($this->backup_folder."/".$this->filename.".gz",'w');
		//echo $this->backup_folder."\\".$this->filename;
		$gzdata = gzencode($this->create_backup(), 9);
		fwrite($f_output,$gzdata);
	    fclose($f_output);
	    $this->mysqli->close();
	}
}
?>