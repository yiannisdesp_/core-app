<?php
    require_once('../../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <script type="text/javascript" src="<?php echo ABS_URL; ?>lib/js/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo ABS_URL; ?>lib/js/plugins/tinymce/load_full.js"></script>
    <script>
        $(document).ready(function(){
            $('a[href^="#<?php echo $langs[0];?>-pills"]').parent('li').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('in');
        });
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Products: Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-barcode fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($app->db->select('products','id',array('active'=>1))); ?></div>
                                    <div>Total products</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>