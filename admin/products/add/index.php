<?php
    require_once('../../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## Get languages
    $langs = $app->content->get_languages();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <script type="text/javascript" src="<?php echo ABS_URL; ?>lib/js/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
       init_tiny_mce('300px','100%','modern','textarea');
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Products: Add</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-barcode fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($app->db->select('products','id',array('active'=>1))); ?></div>
                                    <div>Total products</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <form method="post" action="../add/">
                <table class='table display'>
                    <tbody>
                        <tr>
                            <td>Set Supplier</td>
                            <td>
                            <select name="supplier" class='form-control'>
                                <?php
                                    $suppliers = $app->db->select('product_suppliers','*',array('active'=>1));
                                    foreach ($suppliers as $supplier) {
                                        echo "<option value='".$supplier['id']."'>".$supplier['name']."</option>";
                                    }
                                ?>
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Set Category&nbsp;<a href='<?php echo ADMIN_ABS_URL;?>products/categories/add/' title='Add category'><i class='fa fa-plus fa-lg'></i></a></td>
                            <td>
                            <select name="supplier" class='form-control'>
                                <?php
                                    $categories = $app->db->select('product_categories','*',array('active'=>1));
                                    foreach ($categories as $category) {
                                        echo "<option value='".$category['id']."'>".$category['name']."</option>";
                                    }
                                ?>
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Set Name</td>
                            <td>
                                <input type="text" name="name" class="form-control"/>
                            </td>
                        </tr>
                        <?php
                            /* Foreach language create the corresponding language input */
                            foreach ($langs as $lang):
                        ?>
                        <tr>
                            <td>Write Product Description</td>
                            <td>
                                <textarea style='width:100%;height:250px' name="description*<?php echo $lang;?>"></textarea>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </form>
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>