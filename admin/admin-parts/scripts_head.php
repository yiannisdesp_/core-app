<!-- jQuery Version 1.11.0 -->
<script src="<?php echo ABS_URL; ?>lib/js/plugins/jquery/jquery-1.11.1.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo ABS_URL; ?>lib/js/bootstrap/bootstrap.min.js"></script>
<!-- jQuery-ui Version 1.11.0 -->
<script src="<?php echo ABS_URL; ?>lib/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- DataTables -->
<script src="<?php echo ABS_URL; ?>lib/js/plugins/dataTables/jquery.dataTables.min.js"></script>
<script src="<?php echo ABS_URL; ?>lib/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<!-- App misc functions -->
<script type="text/javascript" src="<?php echo ABS_URL; ?>lib/js/app/misc.js"></script>