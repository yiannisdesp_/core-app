<?php 
	$admin_data = $app->push_array_up($app->db->select('users','*',array('id'=>$app->get_logged_in_user_id())));
?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html"><?php echo SITE_TITLE; ?> administrator panel</a>
</div>
<!-- /.navbar-header -->
<?php include('top_menu.php'); ?>
<?php include('side_menu.php'); ?>
<?php $app->breadcrumbs(); ?>
</nav>
