<!-- Bootstrap Core CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- DataTables CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<!-- sb-admin-2 CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/sb-admin-2/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<?php // Load fonts from the admin url in order to solve the cross origin issue ?>
<link href="<?php echo ADMIN_ABS_URL; ?>plugins/font-awesome-4.1.0/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Timeline CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/plugins/timeline/timeline.css" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/plugins/morris/morris.css" rel="stylesheet">

<!-- Notifications CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/notifications/notifications.css" rel="stylesheet">
<!-- Jquery UI CSS -->
<link href="<?php echo ABS_URL; ?>lib/css/plugins/jquery-ui/jquery-ui.structure.min.css" rel="stylesheet">
<link href="<?php echo ABS_URL; ?>lib/css/plugins/jquery-ui/jquery-ui.blue-theme.min.css" rel="stylesheet">