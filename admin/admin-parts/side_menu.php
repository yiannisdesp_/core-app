<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <?php /*
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            */ ?>
            <li>
                <a class="active" href="<?php echo ADMIN_ABS_URL; ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Content Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>content-editing/about/">About</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>content-editing/faqs/">FAQs</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>content-editing/contact-us/">Contact us</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>content-editing/">All</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-barcode fa-fw"></i> Products Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>products/add/">Add</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>products/edit/">Edit</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>products/remove/">Remove</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <?php 
                ## These pages are only available to admin users
                if($app->logged_in_user('level') == 'admin'):
            ?>
            <li>
                <a href="#"><i class="fa fa-database fa-fw"></i> System's DB backup<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>backup?download">Download</a>
                    </li>
                    <li>
                        <a href="#" onClick="$.get('<?php echo ADMIN_ABS_URL; ?>backup?store');$('#confirm-store').dialog();">Store</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_ABS_URL; ?>backup/view/">View backups</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <?php endif; ?>
            <li>
                <a class="active" href="<?php echo ADMIN_ABS_URL; ?>slider/"><i class="fa fa-camera fa-fw"></i> Slider</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->

<!-- confirm store dialog -->
<div id="confirm-store" title="Stored" style="display:none">
  <p>The backup created successfully.</p>
  <p>You can view all backups <a href="<?php echo ADMIN_ABS_URL;?>backup/view/">here</a></p>
</div>