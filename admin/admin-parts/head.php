<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- favicons/touch icons -->
<link rel="shortcut icon" href="<?php echo IMGS_URL; ?>icos/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo IMGS_URL; ?>icos/ico-144.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo IMGS_URL; ?>icos/ico-114.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo IMGS_URL; ?>icos/ico-72.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo IMGS_URL; ?>icos/ico-57.png">
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/css.php'); ?>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_head.php'); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->