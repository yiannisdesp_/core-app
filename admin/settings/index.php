<?php
    require_once('../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## This page is only available to admin users
    if(!$app->logged_in_user('level') == 'admin') $app->redirect_to(ADMIN_ABS_URL);
    ## ----------------------------------------
    ## Post data handling - profile update
    if(isset($_POST)){
        foreach ($_POST as $key => $value) {
            $app->db->update('config',array('value'=>$value),array('setting'=>$key));
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Settings</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="table-responsive">
                <table class="table">
                <form method="post" action="../settings/">
                <?php
                    $settings = $app->db->select('config','*');
                    foreach($settings as $setting){
                        echo "<tr><td>".$app->readable_field($setting['setting'])."</td>";
                        echo "<td><input class='form-control' type='text' value='".$setting['value']."' name='".$setting['setting']."'</td></tr>";
                    }
                ?>
                <tr><td></td><td><input type="submit" class="btn btn-primary" value="Update details"/></td></tr>
                </form>
                </table>
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>