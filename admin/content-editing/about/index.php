<?php
    require_once('../../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## Get languages
    $langs = $app->content->get_languages();
    ## ----------------------------------------
    ## Post data handling - content update
    if(isset($_POST['submit'])){
        foreach ($_POST as $key => $value) {
            if($key !== 'submit'){
                $temp = explode("*", $key);
                $lang_field = $temp[0];
                $keyword = $temp[1];
                
                $app->db->update('content',array($lang_field=>$value),array('keyword'=>$keyword));
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <script type="text/javascript" src="<?php echo ABS_URL; ?>lib/js/plugins/tinymce/tinymce.min.js"></script>
    <script>
        init_tiny_mce('400px','100%','modern','textarea');
        $(document).ready(function(){
            $('a[href^="#<?php echo $langs[0];?>-pills"]').parent('li').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('in');
        });
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Content editing: About</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-edit fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($langs); ?></div>
                                    <div>Total languages</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span>Choose language: </span>
                <ul class="nav nav-pills">
                    <?php foreach ($langs as $lang): ?>
                    <li class=""><a href="#<?php echo $lang; ?>-pills" data-toggle="tab"><?php echo $app->readable_field($lang);?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- /.row -->
            <?php
                ## Get about content
                // put 'keyword' field to the beginning of the array
                $fields = $langs;
                array_unshift($fields,'keyword');
                $about_content = $app->content->get_translations('about_text');
            ?>
            <div class="row">
                <form method='post' action='../about/'>
                    <div class="tab-content">
                    <?php foreach ($langs as $lang): ?>
                        <div class="tab-pane fade" id="<?php echo $lang;?>-pills">
                        <h4>Content language: <?php echo $app->readable_field($lang); ?></h4>
                        <textarea name='<?php echo $lang;?>*about_text'><?php echo $about_content[$lang]; ?></textarea>
                        </div>    
                    <?php endforeach; ?>
                    </div>
                    <br/>
                    <input name="submit" type="submit" value="Update" class="btn btn-primary" style="float:right">
                </form>
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>