<?php
    require_once('../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## Get languages
    $langs = $app->content->get_languages();
    /*==========================================
    =            Post Data handling            =
    ==========================================*/

    /*==========  Content update  ==========*/
    if(isset($_POST['submit'])){
        foreach ($_POST as $key => $value) {
            if($app->string_contains($key,'*')){
                $temp = explode("*", $key);
                $lang = $temp[0];
                $keyword = $temp[1];
                $app->db->update('content',array($lang=>$value),array('keyword'=>$keyword));
            }
        }
    }
    /*==========  Create new content  ==========*/
    if(isset($_POST['new-keyword'])){
        $data = array('keyword'=> $_POST['new-keyword']);
        if(!$app->if_string_empty($data['keyword'])){
            ## iterate each language
            foreach ($_POST as $key => $value) {
                if($app->string_contains($key,'*new-content')){
                    $temp = explode('*', $key);
                    $lang = $temp[0];
                    $data[$lang] = $value;
                }
            }
            ## insert data to db
            $app->db->insert('content',$data);
        }else{
            $app->alert_box('Keyword must not be empty','error');
        }
    }

    /*-----  End of Post Data handling  ------*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.content-table').dataTable({responsive: true});
            $('a[href^="#<?php echo $langs[0];?>-pills"]').parent('li').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('in');
        });
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Content editing: All</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-edit fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($langs); ?></div>
                                    <div>Total languages</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <span>Choose language: </span>
                <ul class="nav nav-pills">
                    <?php foreach ($langs as $lang): ?>
                    <li class=""><a href="#<?php echo $lang; ?>-pills" data-toggle="tab"><?php echo $app->readable_field($lang);?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- /.row -->
            <!-- Tabbed content -->
            <div class="tab-content">
            <?php foreach ($langs as $lang): ?>
            <div class="table-responsive tab-pane fade" id="<?php echo $lang;?>-pills">
            <form method="post" action="../content-editing/">
                <table class="table display content-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Keyword</th>
                    <th><?php echo $lang; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    /* Select all content where large_text is false, and table_name and table_id is null */
                    $content = $app->db->select('content','*',array('AND'=>array('large_text'=>0,'table_name' => null,'table_id' => null)));
                    foreach($content as $row){
                        echo "<tr><td>".$app->readable_field($row['id'])."</td>";
                        echo "<td>".$row['keyword']."</td>";
                        echo "<td><input class='form-control' type='text' value='".$row[$lang]."' name='".$lang."*".$row['keyword']."'/></td>";
                        echo "</tr>";
                    }
                ?>
                </tbody>
                </table>
                <input type="submit" name="submit" class="btn btn-primary" value="Update" style="float:left;"/>
                </form>
            </div>
            <?php endforeach; ?>
            </div><!-- /tabbed content -->
            <?php if($app->logged_in_user('level') == 'admin'): ?>
            &nbsp;&nbsp;&nbsp;<span class='btn btn-warning' data-toggle="modal" data-target="#create-new">Create new entry</span>
            <?php endif; ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Create new entry Modal -->
    <!-- Modal -->
    <div class="modal fade" id="create-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Create new entry</h4>
          </div>
          <div class="modal-body">
            <form method="post" action='../content-editing/' id="create-new-content-form">
                <table class="table display">
                    <tr><td>Keyword</td><td><input type="text" name="new-keyword" class='form-control'></td></tr>
                    <?php foreach ($langs as $lang): ?>
                    <tr><td><?php echo $app->readable_field($lang);?></td><td><input type="text" name="<?php echo $lang;?>*new-content" class="form-control"></td></tr>
                    <?php endforeach; ?>
                </table>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="document.getElementById('create-new-content-form').submit();">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /Modal -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>