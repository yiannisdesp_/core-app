<?php
    require_once('../../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## Remove backup file
    if(isset($_GET['rm'])){
        $app->remove_file(ADMIN_ABS_PATH . 'backup/db_backups/' . $_GET['rm']);
        $app->redirect_to(ADMIN_ABS_URL . 'backup/view/');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#content-table').dataTable();
        });
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View database backup files</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-database fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($app->get_dir_contents(DB_BACKUP_DIR)); ?></div>
                                    <div>Total backups</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
            <table class="table display" id="content-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date created</th>
                        <th>Time created</th>
                        <th>Download</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $files = $app->get_dir_contents(DB_BACKUP_DIR); 
                    foreach ($files as $key => $file) {
                        echo "<tr>";
                        echo "<td>".($key+1)."</td>";
                        $temp = explode("_", $file);
                        $date_created = $temp[2];
                        $time_created = $temp[3] . ":" . $temp[4] . ":" . $temp[5][0].$temp[5][1];
                        echo "<td>".$date_created."</td>";
                        echo "<td>".$time_created."</td>";
                        echo "<td><a href='".DB_BACKUPS_URL.$file."'><span class='fa fa-download fa-2x'></span></a></td>";
                        echo "<td><a href='".ADMIN_ABS_URL."backup/view/?rm=".$file."'><span class='fa fa-times fa-2x'></span></a></td>";
                        echo "</tr>";
                    }
                ?>
                </tbody>
            </table>
            </div>
        
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>