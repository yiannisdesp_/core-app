-- pjl SQL Dump
-- Server version:5.6.16
-- Generated: 2014-09-27 02:07:32
-- Current PHP version: 5.5.11
-- Host: localhost
-- Database:core_app
-- --------------------------------------------------------
-- Structure for 'config'
--

DROP TABLE IF EXISTS config;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting` varchar(100) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `config`
--

INSERT INTO config (`id`,`setting`,`value`) VALUES ("1","abs_url","http://localhost/core_app/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("2","cookies_path","/core_app");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("3","abs_path","C:/xampp/htdocs/core_app/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("4","db_backup_dir","C:/xampp/htdocs/core_app/admin/backup/db_backups");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("5","imgs_path","C:/xampp/htdocs/core_app/images/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("6","logo_path","C:/xampp/htdocs/core_app/images/logo.png");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("7","logo_header_path","C:/xampp/htdocs/core_app/images/header_logo.png");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("8","admin_abs_path","C:/xampp/htdocs/core_app/admin/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("9","error_404_url","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("10","error_403_url","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("11","error_500_url","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("12","admin_abs_url","http://localhost/core_app/admin/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("13","site_title","site_title");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("14","ga_tracking_code","UA-55015078-1");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("15","library_path","C:/xampp/htdocs/core_app/lib/");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("16","ga_account_id","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("17","admin_email","0");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("18","admin_tasks","0");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("19","admin_notifications","1");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("20","admin_breadcrumb_anchor_class","btn btn-primary btn-xs");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("21","admin_breadcrumb_div_class","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("22","admin_breadcrumb_home","Dashboard");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("23","breadcrumb_separator"," / ");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("24","breadcrumb_anchor_class","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("25","breadcrumb_div_class","");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("26","breadcrumb_home","Home");
INSERT INTO config (`id`,`setting`,`value`) VALUES ("27","db_backups_url","http://localhost/core_app/admin/backup/db_backups/");



-- --------------------------------------------------------
-- Structure for 'contact_log'
--

DROP TABLE IF EXISTS contact_log;
CREATE TABLE `contact_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `from_email` varchar(100) NOT NULL,
  `from_surname` varchar(100) DEFAULT NULL,
  `from_phone` varchar(50) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `topic` text NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `contact_log`
--




-- --------------------------------------------------------
-- Structure for 'content'
--

DROP TABLE IF EXISTS content;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(200) NOT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `el` text NOT NULL,
  `en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `content`
--

INSERT INTO content (`id`,`keyword`,`table_name`,`table_id`,`el`,`en`) VALUES ("1","who_we_are_lbl","","","????? ???????","Who we are");
INSERT INTO content (`id`,`keyword`,`table_name`,`table_id`,`el`,`en`) VALUES ("2","policy_lbl","","","???? ??????","Policy");
INSERT INTO content (`id`,`keyword`,`table_name`,`table_id`,`el`,`en`) VALUES ("3","about_text","","","<p>&Sigma;&Chi;&Epsilon;&Tau;&Iota;&Kappa;&Alpha; &Kappa;&Epsilon;&Iota;&Mu;&Epsilon;&Nu;&Omicron;</p>","<p>ABOUT TEXT</p>");
INSERT INTO content (`id`,`keyword`,`table_name`,`table_id`,`el`,`en`) VALUES ("4","faqs_text","","","?????? ????????? ???????","FAQS TEXT");
INSERT INTO content (`id`,`keyword`,`table_name`,`table_id`,`el`,`en`) VALUES ("5","contact_us_text","","","????????????? ???? ???","Contact us");



-- --------------------------------------------------------
-- Structure for 'product_categories'
--

DROP TABLE IF EXISTS product_categories;
CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `product_categories`
--




-- --------------------------------------------------------
-- Structure for 'product_suppliers'
--

DROP TABLE IF EXISTS product_suppliers;
CREATE TABLE `product_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `logo` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `product_suppliers`
--




-- --------------------------------------------------------
-- Structure for 'products'
--

DROP TABLE IF EXISTS products;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `p_category_id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `products`
--




-- --------------------------------------------------------
-- Structure for 'users'
--

DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `surname` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(300) NOT NULL,
  `phone_no` int(11) DEFAULT NULL,
  `level` set('admin','manager','customer','editor','user') NOT NULL DEFAULT 'user',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL,
  `last_login_ip` varchar(100) NOT NULL,
  `session_active` int(11) NOT NULL DEFAULT '0',
  `total_logins` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
-- Dump Data for `users`
--

INSERT INTO users (`id`,`name`,`surname`,`password`,`username`,`email`,`phone_no`,`level`,`created`,`modified`,`last_login`,`last_login_ip`,`session_active`,`total_logins`,`active`) VALUES ("1","admin","admin","d033e22ae348aeb5660fc2140aec35850c4da997","admin","admin@admin.com","99999999","admin","0000-00-00 00:00:00","0000-00-00 00:00:00","2014-09-26 10:04:40","::1","1","13","1");



