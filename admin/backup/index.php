<?php 
	include('../../lib/php/init.php');
	if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## This page is only available to admin users
    if(!$app->logged_in_user('level') == 'admin') $app->redirect_to(ADMIN_ABS_URL);
    ## ----------------------------------------
    ## Backup function behavior
	if(isset($_GET['download'])){
		$app->backup_db();
	}elseif(isset($_GET['store'])){
		$app->backup_db('store');
	}else{
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
	    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
	</head>

	<body>
	    <div id="wrapper">
	        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>
	        <div id="page-wrapper">
	            <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header">Page restricted</h1>
	                </div>
	                <!-- /.col-lg-12 -->
	            </div>
	            <!-- /.row -->
	            <?php $app->alert_box('This page has restricted access','error'); ?>
	            
	        </div>
	        <!-- /#page-wrapper -->

	    </div>
	    <!-- /#wrapper -->
	</body>
	</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>
	<?php
	}
?>