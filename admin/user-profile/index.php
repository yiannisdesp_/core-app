<?php
    session_start();
    require_once('../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## Post data handling - profile update
    $fields_can_be_edited = array('name','surname','password','phone_no','username');
    if(isset($_POST)){
        // password field:
        if(isset($_POST['password']) && $_POST['password'] == ''){
            unset($_POST['password']);
        }else{
            if(isset($_POST['password']) && !($_POST['password'] == '')) $_POST['password'] = sha1($_POST['password']);
        }
        $app->db->update('users',$_POST,array('id'=>$app->get_logged_in_user_id()));
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User profile</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class=""><?php echo $admin_data['total_logins']; ?></div>
                                    <div>Times logged in</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class=""><?php echo $admin_data['last_login']; ?></div>
                                    <div>Last login</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class=""><?php echo $app->get_client_ip(); ?></div>
                                    <div>Current IP</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <div class="table-responsive">
                <table class="table">
                <form method="post" action="../user-profile/">
                <?php
                    foreach ($admin_data as $field => $value){
                        if(in_array($field, $fields_can_be_edited)){
                            if($field == 'password'){ 
                                $input_type = 'password'; 
                                $value = ''; 
                                $title = 'Password will remain unchanged if blank'; 
                            }else{ $input_type = 'text'; $title = ''; }
                            echo "<tr><td>".$app->readable_field($field)."</td>";
                            echo "<td><input class='form-control' title='".$title."' type='".$input_type."' value='".$value."' name='".$field."'</td></tr>";
                        }else{
                            echo "<tr><td>".$app->readable_field($field)."</td><td>".$value."</td></tr>";
                        }
                        
                    }
                ?>
                <tr><td></td><td><input type="submit" class="btn btn-primary" value="Update details"/></td></tr>
                </form>
                </table>
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>