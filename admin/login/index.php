<?php 
    session_start();
    require_once('../../lib/php/init.php');
    $error = '';
    if(isset($_POST['email']) && isset($_POST['password'])) {
        if($app->login($_POST['email'],$_POST['password'],'session')){
            $app->redirect_to(constant('ADMIN_ABS_URL'));
        }else{
            $error = "<span class='label label-warning'>Log in error</span>";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin - <?php echo SITE_TITLE; ?></title>
    <?php include(ADMIN_ABS_PATH . 'admin-parts/css.php'); ?>
    <?php include(ADMIN_ABS_PATH . 'admin-parts/scripts_head.php'); ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo SITE_TITLE; ?> - Admin Panel - Please Log In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action='index.php' method='post'>
                            <fieldset>
                                <div class="form-group">
                                    <?php if(isset($_POST['email'])) {$email_val = $_POST['email'];} else {$email_val = '';} ?>
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" value="<?php echo $email_val; ?>" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <?php /*
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                */ ?>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type='submit' value='Login' class="btn btn-lg btn-success btn-block"/>
                            </fieldset>
                            <?php if(!$app->if_string_empty($error)) echo $error; ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php include(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>
