<?php
    require_once('../../lib/php/init.php');
    if(!$app->authenticate_user('session')) $app->redirect_to(ADMIN_ABS_URL . 'login/');
    ## ----------------------------------------
    ## Get languages
    $langs = $app->content->get_languages();
    ## ----------------------------------------
    /*===========================================================
    =                      Post data handling                   =
    ===========================================================*/
    ## .1 Insert new image
    // A list of permitted file extensions
    $allowed = array('png', 'jpg', 'gif');
    if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
        $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        if(!in_array(strtolower($extension), $allowed)){
            $err_str = 'Extension not allowed. Allowed extensions: ';
            foreach ($allowed as $ext) {$err_str .= strtoupper($ext) . ', ';}
            $err_str = rtrim(", ",$err_str);
            $app->alert_box($err_str);
        }
        if(move_uploaded_file($_FILES['upl']['tmp_name'], SLIDER_IMGS_ABS_DIR.'/'.$_FILES['upl']['name'])){
            $temp = explode('.', $_FILES['upl']['name']);
            $filename = $temp[0];
            $gen_keyword_title = 'slider_img_title_' . $filename;
            $gen_keyword_description = 'slider_img_description_' . $filename;
            $gen_keyword_link = 'slider_img_link_' . $filename;
            $app->db->insert('content',array(array('keyword'=>$gen_keyword_title),array('keyword'=>$gen_keyword_description),array('keyword'=>$gen_keyword_link)));
            foreach ($langs as $lang) { $app->db->update('content',array($lang=>'#'),array('keyword'=>$gen_keyword_link)); }
        }
    }
    ## 2. Remove an image
    if(isset($_GET['rm'])){
        $temp = explode('.', $_GET['rm']);
        // Delete from db
        $app->db->delete('content',array('keyword' => 'slider_img_title_' . $temp[0]));
        $app->db->delete('content',array('keyword' => 'slider_img_description_' . $temp[0]));
        $app->db->delete('content',array('keyword' => 'slider_img_link_' . $temp[0]));
        // delete file
        if(!$app->remove_file(SLIDER_IMGS_ABS_DIR.'/'.$_GET['rm'])){
            $app->alert_box('Image not found.');
        }else{
            $app->redirect_to(ADMIN_ABS_URL . 'slider/?removed');
        }
    }
    ## 3. Update Data
    if(isset($_POST['submit-data'])){
        foreach ($_POST as $key => $value) {
            if(!$app->string_contains($key,'DataTables') && $key !== 'submit-data'){
                // Update db
                $temp = explode('*', $key);
                $lang = $temp[0]; $keyword = $temp[1];
                $app->db->update('content',array($lang=>$value),array('keyword'=>$keyword));
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(ADMIN_ABS_PATH . 'admin-parts/head.php'); ?>
    <!-- file upload css -->
    <link href="<?php echo ABS_URL;?>lib/css/plugins/ajax-file-upload/style.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function() {
            $('.content-table').dataTable({responsive: true});
            $('a[href^="#<?php echo $langs[0];?>-pills"]').parent('li').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('active');
            $('#<?php echo $langs[0];?>-pills').addClass('in');
        });
    </script>
</head>

<body>
    <div id="wrapper">
    
        <?php include(ADMIN_ABS_PATH . 'admin-parts/navigation.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Slider images</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php if(isset($_GET['img_uploaded'])) $app->alert_box('Success.','success'); ?>
            <?php if(isset($_GET['removed'])) $app->alert_box('Image removed.','success'); ?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-camera fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo count($app->get_dir_contents(SLIDER_IMGS_ABS_DIR)); ?></div>
                                    <div>Total images</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <span>Choose language: </span>
                <ul class="nav nav-pills">
                    <?php foreach ($langs as $lang): ?>
                    <li class=""><a href="#<?php echo $lang; ?>-pills" data-toggle="tab"><?php echo $app->readable_field($lang);?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- /.row -->
            <br class="clearfix"/>
            <div class="">
                <form id="upload" method="post" enctype="multipart/form-data">
                    <div id="drop">
                        Drop Here<br class="clearix">
                        <a>Browse</a>
                        <input type="file" name="upl" multiple />
                    </div>
                    <ul>
                        <!-- The file uploads will be shown here -->
                    </ul>
                </form>
            </div>
            <br class="clearfix"/>
            <!-- Begin form Wrapper -->
            <div class="row" style="margin-top:20px;">
            <!-- Tabbed content -->
            <div class="tab-content">
            <?php foreach ($langs as $lang): ?>
            <div class="tab-pane fade" id="<?php echo $lang;?>-pills">
            <form method="post"/>
            <input class='btn btn-primary' type="submit" value="Update data" name="submit-data"/>
            <br class="clearfix"/><br class="clearfix"/>
                <table class="table display content-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Path</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Link</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                <?php $c = 1; foreach ($app->get_dir_contents(SLIDER_IMGS_ABS_DIR) as $image): ?>
                <?php
                    ## Get image title and description content
                    $temp = explode('.', $image);
                    $image_name = $temp[0];
                    $image_title =  $app->content->get_translation('slider_img_title_'.$image_name,$lang);
                    $image_description = $app->content->get_translation('slider_img_description_'.$image_name,$lang);
                    $image_link = $app->content->get_translation('slider_img_link_'.$image_name,$lang);
                ?>
                    <tr>
                        <td><?php echo $c; ?></td>
                        <td><?php echo $image; ?></td>
                        <td><input value="<?php echo $image_title; ?>" type="text" name="<?php echo $lang;?>*slider_img_title_<?php echo $image_name; ?>"/></td>
                        <td><input value="<?php echo $image_description; ?>" type="text" name="<?php echo $lang;?>*slider_img_description_<?php echo $image_name; ?>"/></td>
                        <td><input value="<?php echo $image_link; ?>" type="text" name="<?php echo $lang;?>*slider_img_link_<?php echo $image_name; ?>"/></td>
                        <td><a href='<?php echo ADMIN_ABS_URL;?>slider/?rm=<?php echo $image; ?>'><span class='fa fa-times fa-2x'></span></a></td>
                    </tr>  
                <?php $c++; endforeach; ?>
                </tbody>
                </table>
                </form><!-- form -->
                </div>
                <?php endforeach; ?>
                </div><!-- tabbed content -->
            </div>
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>
<?php include_once(ADMIN_ABS_PATH . 'admin-parts/scripts_bottom.php'); ?>
<!-- jQuery File Upload Dependencies -->
<script src="<?php echo ABS_URL;?>lib/js/plugins/jquery/jquery.knob.js"></script>
<script src="<?php echo ABS_URL;?>lib/js/plugins/jquery/jquery.ui.widget.js"></script>
<script src="<?php echo ABS_URL;?>lib/js/plugins/jquery/jquery.iframe-transport.js"></script>
<script src="<?php echo ABS_URL;?>lib/js/plugins/jquery/jquery.fileupload.js"></script>
<!-- main JS file -->
<script src="<?php echo ABS_URL;?>lib/js/plugins/ajax-file-upload/ajax-file-upload.js"></script>