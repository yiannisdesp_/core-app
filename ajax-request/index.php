<?php
	require_once('../lib/php/init.php');

	/*==========  AJAX REQUESTS  ==========*/
	
	/*=======================================
	=            Change language            =
	=======================================*/
	if(isset($_POST['chng_lang'])){
		$app->content->set_lang($_POST['chng_lang']);
	}
	/*-----  End of Change language  ------*/


	/*================================================================================
	=            IF NO POST REQUESTS FOUND REDIRECT THE USER TO HOME PAGE            =
	================================================================================*/
	if(!sizeof($_POST)) $app->redirect_to(ABS_URL);	
	/*-----  End of IF NO POST REQUESTS FOUND REDIRECT THE USER TO HOME PAGE  ------*/
	
	
	
	
?>